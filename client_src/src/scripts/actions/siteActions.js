export const SITE_STARTED = 'SITE_STARTED';

export function siteStarted() {
  return {
    type: SITE_STARTED
  };
}

import postApi from '../api/postApi';

export const GET_STARTED = 'GET_STARTED';
export const GET_STARTED_BY_AN_EVENT = 'GET_STARTED_BY_AN_EVENT';
export const POSTS_FETCH = 'POSTS_FETCH';

// action creators
export const getStarted = () => ({ type: GET_STARTED })
export const getStartedByAnEvent = () => ({ type: GET_STARTED_BY_AN_EVENT })
export const getPosts = (posts) => ({
    type: POSTS_FETCH,
    posts
})

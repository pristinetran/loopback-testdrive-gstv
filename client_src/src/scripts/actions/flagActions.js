export const FLAG_STARTED = 'FLAG_STARTED';

export function flagStarted() {
  return {
    type: FLAG_STARTED
  };
}

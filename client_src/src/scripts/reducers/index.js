import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import {rootReducer as userReducer} from '../store/userStore';

import appReducer from './appReducer';

export default combineReducers({
  appReducer,
  routing: routerReducer,
  userReducer
});

import { GET_STARTED, GET_STARTED_BY_AN_EVENT, POSTS_FETCH } from '../actions/appActions';
import { WELCOME_TEXT, WELCOME_TEXT_BY_AN_EVENT } from '../constants';

function appReducer(state = {}, action) {
  switch (action.type) {
    case GET_STARTED:
      return Object.assign({}, state, {status: 'resolved', welcome: WELCOME_TEXT});
    case GET_STARTED_BY_AN_EVENT:
      return Object.assign({}, state, {status: 'resolved', event: WELCOME_TEXT_BY_AN_EVENT});
    case POSTS_FETCH:
      return {status:'requesting'};
    // case POSTS_FETCH_
    default:
      return state;
  }
}

export default appReducer;

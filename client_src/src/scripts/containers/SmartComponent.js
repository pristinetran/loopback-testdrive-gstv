import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {actions as userActions} from '../store/userStore';
import { push } from 'react-router-redux'
import DumbComponent from '../components/DumbComponent';

class SmartComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    }
  }

  componentDidMount() {
    const {fetchUsers} = this.props;
    fetchUsers().then(data =>{
      this.setState({users: data.body});
    });
  }

  render() {
    const {changePage} = this.props;
    console.log("PROPS", this.props);

    return (
      <div>
        <div>
          <DumbComponent users={this.state.users}/>
          <p>
            <button onClick={changePage}>Go to about page via redux</button>
          </p>
        </div>
      </div>
    );
  }
}

export default connect(
  // mapStateToProps
  state => ({users: state.items}),
  // mapDispatchToProps
  dispatch => bindActionCreators({
    ...userActions,
    changePage: () => push('/site')
  }, dispatch),

)(SmartComponent);

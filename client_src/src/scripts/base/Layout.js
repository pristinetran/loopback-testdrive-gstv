// SmartComponent

import React, { Component } from 'react';
import Header from './main/Header';
import Body from './main/Body';
import Footer from './main/Footer';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {withRouter} from 'react-router-dom'

import * as appActions from '../actions/appActions';
import * as siteActions from '../actions/siteActions';

class Layout extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.appActions.getStarted()
  }

  render() {
    const {appActions, appReducer, getStartedByAnEvent} = this.props;

    console.warn('welcomeText', this.props);
    return (
      <div className="App">
        <Header />


        Invoking getStarted:
        <p><code>
          {appReducer.welcome}
        </code></p>

        <button onClick={appActions.getStartedByAnEvent}>Render data by event</button>
        <p><code>
          {appReducer.event}
        </code></p>


        <Body/>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch),
  siteActions: bindActionCreators(siteActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout))

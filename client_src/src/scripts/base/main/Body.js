import React from 'react';
import Routes from '../../routes/Routes';

const Body = (props) => (
  <div className="ui container">
    <h2>Page content</h2>
    <Routes />
  </div>
);

export default Body;

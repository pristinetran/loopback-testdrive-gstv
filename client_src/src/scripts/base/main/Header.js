import React, { Component } from 'react';
import NavBar from '../NavBar';

export default class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <NavBar />
      </header>
    );
  }
}

import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';

export default class Footer extends Component {
  render() {
    return (
      <footer className="App-footer">
        <Grid>
          <Grid.Column floated='left' width={5}>
          </Grid.Column>
          <Grid.Column floated='right' width={5}>
            <small>
              Developed by <i>Phuc Tran</i>
            </small>
          </Grid.Column>
        </Grid>
      </footer>
    );
  }
}

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';
import FontAwesome from 'react-fontawesome';

class NavBar extends Component {
  state = {};
  handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  render() {
    const { activeItem } = this.state;
    return (
      <Menu stackable>
        <Menu.Item
          as={Link}
          name="home"
          to="/"
          active={activeItem === 'home'}
          onClick={this.handleItemClick}
        >
          <FontAwesome
            className='super-crazy-colors'
            name='bars'
            size='2x'
            style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
          />
        </Menu.Item>

        <Menu.Item
          as={Link}
          name="user"
          to="user"
          active={activeItem === 'user'}
          onClick={this.handleItemClick}
        >
          User
        </Menu.Item>

        <Menu.Item
          as={Link}
          name="site"
          to="site"
          active={activeItem === 'site'}
          onClick={this.handleItemClick}
        >
          Site
        </Menu.Item>

        <Menu.Item
          as={Link}
          name="about"
          to="about"
          active={activeItem === 'about'}
          onClick={this.handleItemClick}
        >
          About
        </Menu.Item>

        <Menu.Item
          as={Link}
          name="upcoming-events"
          to="upcoming-events"
          active={activeItem === 'upcoming-events'}
          onClick={this.handleItemClick}
        >
          Upcoming Events
        </Menu.Item>
      </Menu>
    );
  }
}

export default NavBar;

import axios from 'axios';
const postUrl = 'https://jsonplaceholder.typicode.com/posts';

export default class postApi {
  static getAllPosts(){
    return axios.get(postUrl)
      .then(function (response) {
        console.log('getAllPosts resolve', response);
        return response.data;
      })
      .catch(function (error) {
        console.log('getAllPosts reject', error);
      });
  }
}

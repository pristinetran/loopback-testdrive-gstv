import React from 'react';
import { Switch, Route } from 'react-router-dom';

import User from '../components/User';
import About from '../components/About';
import Site from '../components/Site';
import UpcomingEvents from '../components/UpcomingEvents';
import Home from '../components/Home';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/user" component={User} />
    <Route path="/about" component={About} />
    <Route path="/site" component={Site} />
    <Route path="/upcoming-events" component={UpcomingEvents} />
  </Switch>
);

export default Routes;

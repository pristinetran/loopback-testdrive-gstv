import {createResource} from 'redux-rest-resource';

const hostUrl = 'https://jsonplaceholder.typicode.com/users';

export const {types, actions, rootReducer} = createResource({
  name: 'user',
  url: `${hostUrl}/:id`
});

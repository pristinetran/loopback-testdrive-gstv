import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory'
import promiseMiddleware from 'redux-promise-middleware';
import { routerMiddleware } from 'react-router-redux'
import rootReducer from '../reducers';
export const history = createHistory();

export default function configureStore(initialState) {
  const middlewares = [promiseMiddleware(), thunk, routerMiddleware(history)];

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension ? window.devToolsExtension() : f => f // add support for Redux dev tools
    )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default; // eslint-disable-line global-require
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

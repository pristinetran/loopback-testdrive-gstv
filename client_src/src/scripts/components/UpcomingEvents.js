import React from 'react';
import UpcomingEventsRoutes from '../routes/UpcomingEventsRoutes';
import SmartComponent from '../containers/SmartComponent';

const UpcomingEvents = () => (
  <div>
    <span>an extra module</span>
    <UpcomingEventsRoutes />
    <SmartComponent />
  </div>
);
export default UpcomingEvents;

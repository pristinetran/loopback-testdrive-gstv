import React, { Component } from 'react';

const UserListItem = props => {
  return (
    <li>{props.user.name}</li>
  )
};

// <UserListItem key={user.id} user={user} {...actions} />
const DumbComponent = props => (
  <ol>
    {props.users.map(user => <UserListItem key={user.id} user={user} />)}
  </ol>
);

export default DumbComponent;


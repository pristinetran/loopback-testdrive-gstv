import React from 'react';

const Home = () => (
  <div>
    <h3>Welcome to Home Page!</h3>

    <small>You are running this application in <b>{process.env.NODE_ENV}</b> mode.</small>

  </div>
);

export default Home;

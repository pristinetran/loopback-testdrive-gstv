// import PropTypes from 'prop-types';
import React from 'react';
import { ConnectedRouter } from 'react-router-redux'

import Layout from './scripts/base/Layout';

import { Provider } from 'react-redux';

import configureStore, { history } from './scripts/store/configureStore';
const store = configureStore();

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Layout className="App" />
    </ConnectedRouter>
  </Provider>
);

export default App;
